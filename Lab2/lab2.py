from random import randrange

#win sets - static information

#variables: board space, current player, list of players, winner, move - variables
move = None
board = [" "," "," "," "," "," "," "," "," "]

current_player = ""
winner = None
temp = None
pref = 0

win_cons = ((0, 1, 2),(3, 4, 5),(6, 7, 8),(0, 3, 6),(1, 4, 7),(2, 5, 8),(0, 4, 8),(2, 4, 6))
move_prefs = [4, 0, 2, 6, 8, 1, 3, 5, 7]

#functions:

#check if valid move is made
def valid_move():
    global move
    try:
        move = int(move)
        #check that the space exists
        if move >= 0 or move <= 8: 
            #check that the space is free
            if board[move] is " ":
                return True
            else:
                if current_player is "x":
                    print("That space is taken! Pick another.")
                return False
    except:
        if current_player is "x":
                    print("You must enter a value between 0 and 8. Try again.")
        return False

#check if there will be a win, loss or draw
def check_tobe_winstate():
    #check each row
    for row in win_cons:
        #do any of these rows match? are they all full?
        if board[row[0]] is board[row[1]] is "o" and board[row[2]] is " ":
            return row[2]
        if board[row[2]] is board[row[1]] is "o" and board[row[0]] is " ":
            return row[0]
        if board[row[0]] is board[row[2]] is "o" and board[row[1]] is " ":
            return row[1]
        
        if board[row[0]] is board[row[1]] is "x" and board[row[2]] is " ":
            return row[2]
        if board[row[2]] is board[row[1]] is "x" and board[row[0]] is " ":
            return row[0]
        if board[row[0]] is board[row[2]] is "x" and board[row[1]] is " ":
            return row[1]

#check if there is a win, loss or draw
def check_winstate():
    #check each row
    for row in win_cons:
        #do any of these rows match? are they all full?
        if board[row[0]] is board[row[1]] is board[row[2]] != ' ':
            #who won?
            return board[row[0]]
    if ' ' not in board:
        return "draw"
    #if neither condition is met, continue with the game
    return None

#get moves
def get_human():
    temp = input()
    return temp

def get_ai():
    #random AI
    return randrange(9)

def get_smart():
    #smart ai
    global move_prefs
    global move
    pref = 0
    #try to win or prevent a win
    while pref <= 8 and pref >= 0:
        move = move_prefs[pref]
        if valid_move():
            if check_tobe_winstate():
                i = check_tobe_winstate()
                return int(i)
        pref = pref + 1

    pref = 0
    #otherwise, just pick a spot
    while pref <= 8 and pref >= 0:
        move = move_prefs[pref]
        if valid_move():
                return move_prefs[pref]
        pref = pref + 1

#process input
def getinput():
    global move
    if current_player is "x":
        move = get_human()
    else:
        move = get_smart()

#update model
def update():
    global current_player
    global winner
    if valid_move():
        board[move] = current_player
        winner = check_winstate()
        if current_player is "x":
            current_player = "o"
        else:
            current_player = "x"
        

#render
def render():
    print ("    " + board[0] + " | " + board[1] + " | " + board[2])
    print ("   -----------")
    print ("    " + board[3] + " | " + board[4] + " | " + board[5])
    print ("   -----------")
    print ("    " + board[6] + " | " + board[7] + " | " + board[8])

    if winner is None:
        print("It's " + current_player + "'s turn.")

def help():
    print("To make a move enter a number between 0 - 8 and press enter.")
    print("The number corresponds to a board position as illustrated:")
    print("    0 | 1 | 2")
    print("    ---------")
    print("    3 | 4 | 5")
    print("    ---------")
    print("    6 | 7 | 8")

    print("")

#--------------------main function starts here------------------------

if __name__ == "__main__":
    #intro
    print("Welcome to Tic-Tac-Toe!")
    help()
    
    #set current player to P1
    current_player = "x"
    #render
    render()
    
#gameloop
    while winner is None:
        getinput()
        update()
        render()

#after a win/loss
    if winner is "draw":
        print("It's a draw, that means you're both winners!")
    elif winner is "x":
        print("Human player wins! Congratulations!")
    elif winner is "o":
        print("AI wins, better luck next time!")
